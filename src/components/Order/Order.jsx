import React from "react";
import { useParams } from "react-router-dom";
import { CardItems } from "./card-item/CardItems";
import { Form } from "./form/Form";
import "./order_modules.scss";
export const Order = ({ products }) => {
  const params = useParams();

  const item = products?.find((item) => {
    return item.id === parseInt(params.id);
  });

  return (
    <div className="order-container">
      <div className="title-1">Сделать заказ</div>
      <CardItems item={item} />
      <Form />
    </div>
  );
};
